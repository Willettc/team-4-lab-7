# todo/models.py

from django.db import models

# AbstractToDo superclass inherits Django's models
class Todo(models.Model):
  title = models.CharField(max_length=120)
  description = models.TextField()
  completed = models.BooleanField(default=False)
  # UrgencyType = models.TextChoices('UrgencyType', 'IMMEDIATE EVENTUAL')
  urgency = models.CharField(blank = True, choices=UrgencyType.choices, max_length=10)
  # ImportanceType = models.TextChoices('ImportanceType', 'HIGH LOW')
  importance = models.CharField(blank = True, choices=ImportanceType.choices, max_length=10)

  # return the current value of completed
  def _displayCompleted():
    return completed

class ToDo(Todo):
  
  def _displayCompleted():
    return models.TextChoices('UrgencyType', 'IMMEDIATE EVENTUAL')
