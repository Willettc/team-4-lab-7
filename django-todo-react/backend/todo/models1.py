# todo/models.py

from django.db import models
# Create your models here.

# add this
class Todo(models.Model):
  title = models.CharField(max_length=120)
  description = models.TextField()
  completed = models.BooleanField(default=False)
  UrgencyType = models.TextChoices('UrgencyType', 'IMMEDIATE EVENTUAL')
  urgency = models.CharField(blank = True, choices=UrgencyType.choices, max_length=10)
  ImportanceType = models.TextChoices('ImportanceType', 'HIGH LOW')
  importance = models.CharField(blank = True, choices=ImportanceType.choices, max_length=10)

  def _str_(self):
    return self.title
