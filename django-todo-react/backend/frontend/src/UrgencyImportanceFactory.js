import Urgency from 'Urgency.js';
import Importance from 'Importance.js';

class Factory() {
	// Factory creates the object and returns the object's html code
	function createUrgencyObject() {
		let urgency = Urgency.createUrgency();
		return urgency;
	}
	function createImportanceObject() {
		let importance = Importance.createImportance();
		return importance;
	}
}
