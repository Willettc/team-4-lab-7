// frontend/src/App.js

    import React, { Component } from "react";
    import Modal from "./components/Modal";
    import axios from "axios";

	class PageTime {
		constructor(displayMode){
			this.displayMode = displayMode;
		}
		radMode(){
			changeBackgroundRad();
		}
	}
	
    class App extends Component {
      constructor(props) {
        super(props);
        this.state = {
          viewCompleted: false,
          activeItem: {
            title: "",
            description: "",
            completed: false,
	    urgency: "",
	    importance: ""
          },
          todoList: []
        };
      }
      componentDidMount() {
        this.refreshList();
      }
      refreshList = () => {
        axios
          .get("http://localhost:8000/api/todos/")
          .then(res => this.setState({ todoList: res.data }))
          .catch(err => console.log(err));
      };
      displayCompleted = status => {
        if (status) {
          return this.setState({ viewCompleted: true });
        }
        return this.setState({ viewCompleted: false });
      };
      renderTabList = () => {
        return (
          <div className="my-5 tab-list">
            <span
              onClick={() => this.displayCompleted(true)}
              className={this.state.viewCompleted ? "active" : ""}
            >
              complete
            </span>
            <span
              onClick={() => this.displayCompleted(false)}
              className={this.state.viewCompleted ? "" : "active"}
            >
              Incomplete
            </span>
			<button
				onClick={() => changeBackgroundRad() }
				className="btn btn-danger"
				>
				Rad Mode{" "}
				</button>
          </div>
        );
      };
      renderItems = () => {
        const { viewCompleted } = this.state;
        const newItems = this.state.todoList.filter(
          item => item.completed === viewCompleted
        );
        return newItems.map(item => (
          <li
            key={item.id}
            className="list-group-item d-flex justify-content-between align-items-center"
          >
            <span
              className={`todo-title mr-2 ${
                this.state.viewCompleted ? "completed-todo" : ""
              }`}
              title={item.description}
            >
              {item.title}
            </span>
            <span>
              <button
                onClick={() => this.editItem(item)}
                className="btn btn-secondary mr-2"
              >
                {" "}
                Edit{" "}
              </button>
              <button
                onClick={() => this.handleDelete(item)}
                className="btn btn-danger"
              >
                Delete{" "}
              </button>
            </span>
          </li>
        ));
      };
      toggle = () => {
        this.setState({ modal: !this.state.modal });
      };
      handleSubmit = item => {
        this.toggle();
        if (item.id) {
          axios
            .put(`http://localhost:8000/api/todos/${item.id}/`, item)
            .then(res => this.refreshList());
          return;
        }
        axios
          .post("http://localhost:8000/api/todos/", item)
          .then(res => this.refreshList());
      };
      handleDelete = item => {
        axios
          .delete(`http://localhost:8000/api/todos/${item.id}`)
          .then(res => this.refreshList());
      };
      createItem = () => {
        const item = { title: "", description: "", completed: false, urgency: "", importance: "" };
        this.setState({ activeItem: item, modal: !this.state.modal });
      };
      editItem = item => {
        this.setState({ activeItem: item, modal: !this.state.modal });
      };
      render() {
        return (
          <main className="content">
            <h1 id="h1Id" className="text-uppercase text-center my-4">Todo app</h1>
            <div id="div1" className="row ">
              <div id="div2" className="col-md-6 col-sm-10 mx-auto p-0">
                <div id="div3"  className="card p-3">
                  <div id="div4" className="">
                    <button id="buttonAdd" onClick={this.createItem} className="btn btn-primary">
                      Add task
                    </button>
                  </div>
                  {this.renderTabList()}
                  <ul id="ul1" className="list-group list-group-flush">
                    {this.renderItems()}
                  </ul>
                </div>
              </div>
            </div>
            {this.state.modal ? (
              <Modal
                activeItem={this.state.activeItem}
                toggle={this.toggle}
                onSave={this.handleSubmit}
              />
            ) : null}
          </main>
        );
      }
    }
	function changeBackgroundRad() {
        var body = document.getElementById("body");
        var root = document.getElementById("root");
	var h1Id = document.getElementById("h1Id");
        var div1 = document.getElementById("div1");
	var div2 = document.getElementById("div2");
	var div3 = document.getElementById("div3");
	var div4 = document.getElementById("div4");
	var ul1 = document.getElementById("ul1");
  	body.className="radmode";
	root.className="radmode";
	h1Id.className="radmode text-uppercase text-center my-4";
	div1.className="radmode row ";
	div2.className="radmode col-md-6 col-sm-10 mx-auto p-0";
	div3.className="radmode card p-3";
	div4.className="radmode";
	ul1.className="radmode list-group list-group-flush";
        var li1 = document.getElementsByTagName("LI");
        var numberOfListItems = li1.length;
        for (var i = 0; i < numberOfListItems; i++) {
        li1[i].className="radmode list-group-item d-flex justify-content-between align-items-center";
        }
 
	}
	
    export default App;
